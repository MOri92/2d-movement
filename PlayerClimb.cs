﻿using System.Collections;
using UnityEngine;

public class PlayerClimb : MonoBehaviour
{
    [SerializeField] private float wallJumpDuration = 0.25f;

    private PlayerStateList pState = null;
    private PlayerController pController = null;

    void Start()
    {
        pState = GetComponent<PlayerStateList>();
        pController = GetComponent<PlayerController>();
    }

    void FixedUpdate()
    {
        if (pState.climb)
            StartCoroutine(JumpFromWall());
    }

    private IEnumerator JumpFromWall()
    {
        pController.AddForce(pController.maxSpeedX * - pState.facingDirection, pController.maxSpeedY);
        pState.jumping = true;
        yield return new WaitForSeconds(wallJumpDuration);
        pState.climb = false;
    }
}
