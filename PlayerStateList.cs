﻿using UnityEngine;

public class PlayerStateList : MonoBehaviour
{
    public bool canMove;
    public float xAxis;
    public float yAxis;

    public bool walking;
    public bool running;
    public bool jumping;
    public bool stopJumping;
    public bool dashing;
    public bool walled;
    public bool climb;
    public bool grounded;
    public bool attack;

    public float facingDirection = 1;

    //public bool interact;
    //public bool interacting;
    public bool recoilingX;
    public bool recoilingY;
    //public bool casting;
    //public bool castReleased;
    //public bool onBench;
    //public bool atBench;
    //public bool atNPC;
    //public bool usingNPC;
}
