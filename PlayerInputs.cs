﻿using UnityEngine;

public class PlayerInputs : MonoBehaviour
{
    private float jumpPressed = 0;
    private PlayerStateList pState = null;

    // Start is called before the first frame update
    void Start()
    {
        pState = GetComponent<PlayerStateList>();
    }

    // Update is called once per frame
    void Update()
    {
        GetInputs();
        HasJumpBeenPressed();
    }

    private void GetInputs()
    {
        pState.xAxis = Input.GetAxis("Horizontal");
        pState.yAxis = Input.GetAxis("Vertical");

        #region X Sensitivity
        if (pState.xAxis > 0.3f)
        {
            pState.xAxis = 1;
        }
        else if (pState.xAxis < -0.3f)
        {
            pState.xAxis = -1;
        }
        else
        {
            pState.xAxis = 0;
        }
        #endregion
        #region Y Sensitivity

        if (pState.yAxis > 0.1f)
        {
            pState.yAxis = 1;
        }
        else if (pState.yAxis < -0.1f)
        {
            pState.yAxis = -1;
        }
        else
        {
            pState.yAxis = 0;
        }
        #endregion

        pState.running = Input.GetButton("Run") && pState.grounded;        

        if (Input.GetButtonDown("Jump"))
            jumpPressed = 0.2f;        

        if(Input.GetButtonUp("Jump") && pState.jumping)
            pState.stopJumping = true;        

        if (Input.GetButtonDown("Dash"))
            pState.dashing = true;        

        pState.attack = Input.GetButtonDown("Attack");
    }

    private void HasJumpBeenPressed()
    {
        if (jumpPressed > 0)
        {
            jumpPressed -= Time.deltaTime;
            if (pState.grounded && !pState.stopJumping)
                pState.jumping = true;
            if (pState.walled)
                pState.climb = true;     
        }
    }
}
