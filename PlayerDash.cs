﻿using System.Collections;
using UnityEngine;

public class PlayerDash : MonoBehaviour
{
    [Header("Dash")]
    [SerializeField] private float dashSpeed = 50f;
    [SerializeField] private float dashDuration = 1f;

    private bool canDash = true;

    private PlayerStateList pState = null;
    private PlayerController pController = null;

    void Start()
    {
        pState = GetComponent<PlayerStateList>();
        pController = GetComponent<PlayerController>();
    }

    void FixedUpdate()
    {
        if (canDash && pState.dashing)
            StartCoroutine(Dash());
    }

    private IEnumerator Dash()
    {
        canDash = false;
        pController.AddForce(pState.facingDirection * dashSpeed, 0);
        yield return new WaitForSeconds(dashDuration);
        pState.dashing = false;
        canDash = true;
    }
}
