﻿using System.Collections;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    [Header("Jump")]
    [SerializeField] private float jumpSpeed = 45;
    [SerializeField] private int jumpSteps = 20;

    [Header("Roof Checking")]
    [SerializeField] private Transform roofTransform = null; 
    [SerializeField] private float roofCheckY = 0.2f;
    [SerializeField] private float roofCheckX = 1;
    [SerializeField] private LayerMask roofLayer = 0;

    private int stepsJumped = 0;

    private PlayerStateList pState = null;
    private PlayerController pController = null;

    void Start()
    {
        pState = GetComponent<PlayerStateList>();
        pController = GetComponent<PlayerController>();
    }

    void FixedUpdate()
    {
        if (pState.stopJumping)
            StartCoroutine(StopJump());
        else if (pState.canMove && pState.jumping)
            Jump();
    }

    private void Jump()
    {
        if (stepsJumped < jumpSteps && !Roofed() && !pState.walled)
        {
            pController.AddForce(0, jumpSpeed * Time.fixedDeltaTime);
            stepsJumped++;
        }
        else
        {
            pState.stopJumping = true;
        }
    }

    private IEnumerator StopJump()
    {
        pState.jumping = false;
        yield return new WaitForSeconds(0.2f);
        stepsJumped = 0;
        pState.stopJumping = false;
    }

    private bool Roofed()
    {
        if (Physics2D.Raycast(roofTransform.position, Vector2.up, roofCheckY, roofLayer))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
