﻿using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Movement Limits")]
    public float maxSpeedX = 50f;
    public float maxSpeedY = 50f;

    [SerializeField] private float walkSpeed = 25f;
    [SerializeField] private float runMultiplier = 1.25f;
    [SerializeField] private float airFriction = 1f;
    [SerializeField] private float wallSliding = 22.5f;
    [Space(5)]

    [Header("Ground Checking")]
    [SerializeField] private Transform groundTransform = null; 
    [SerializeField] private float groundCheckY = 0.2f; 
    [SerializeField] private float groundCheckX = 1;
    [SerializeField] private LayerMask groundLayer = 0;
    [Space(5)]

    [Header("Wall Checking")]
    [SerializeField] private Transform wallTransform = null; 
    [SerializeField] private float wallCheckX = 1f;
    [SerializeField] private LayerMask wallLayer = 0;

    private float acceleration = 0;
    private float maxAcceleration = 0;
    private float playerGroundedTimer = 0; 

    private PlayerStateList pState = null;
    private Rigidbody2D rb = null;    

    private void Start()
    {
        pState = GetComponent<PlayerStateList>();
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (pState.canMove)
        {            
            Flip();
            Running();            
        }

        MovementConstraints();
    }

    private void FixedUpdate()
    {
        Grounded();
        Walled();

        if (pState.canMove)
        {
            if (playerGroundedTimer > 0)
            {
                Accelerate();
                Walk();
            }
            else
            {
                MoveOnAir();
            }
        }

        SpeedConstraints();
    }

    private void Walk()
    {
        rb.velocity = new Vector2(walkSpeed * acceleration * pState.facingDirection * Time.fixedDeltaTime, rb.velocity.y);

        if (Mathf.Abs(rb.velocity.x) > 0)
        {
            pState.walking = true;
        }
        else
        {
            pState.walking = false;
        }

        //anim.SetBool("Walking", pState.walking);
    }
    private void Flip()
    {
        if (pState.xAxis > 0)
        {
            transform.localScale = new Vector2(1, transform.localScale.y);
        }
        else if (pState.xAxis < 0)
        {
            transform.localScale = new Vector2(-1, transform.localScale.y);
        }

        pState.facingDirection = transform.localScale.x;
    }
    private void MoveOnAir()
    {
        if(pState.xAxis != 0)
        {
            rb.velocity += new Vector2(walkSpeed * pState.xAxis * Time.fixedDeltaTime, 0);
        }
        else if (Math.Round(rb.velocity.x) == 0)
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
        }
        else if(rb.velocity.x > 0)
        {
            rb.velocity = new Vector2(rb.velocity.x - (airFriction *  Time.fixedDeltaTime), rb.velocity.y);
        }
        else if (rb.velocity.x < 0)
        {
            rb.velocity = new Vector2(rb.velocity.x + (airFriction * Time.fixedDeltaTime), rb.velocity.y);
        }        
    }
    private void Grounded()
    {
        if (Physics2D.Raycast(groundTransform.position, Vector2.down, groundCheckY, groundLayer )
         || Physics2D.Raycast(groundTransform.position + new Vector3(-groundCheckX, 0), Vector2.down, groundCheckY, groundLayer)
         || Physics2D.Raycast(groundTransform.position + new Vector3(groundCheckX, 0), Vector2.down, groundCheckY, groundLayer))
        {   
            playerGroundedTimer = 0.05f;
        }
        else if(playerGroundedTimer > 0)
        {
            playerGroundedTimer -= Time.deltaTime;
        }

        pState.grounded = playerGroundedTimer > 0;
    }
    private void Walled()
    {
        if (Physics2D.Raycast(wallTransform.position, Vector2.right, wallCheckX, wallLayer) && playerGroundedTimer <= 0)
        {
            pState.walled = true;
        }
        else
        {
            pState.walled = false;
        }
    }
    private void Accelerate()
    {
        if (pState.xAxis != 0 && acceleration < maxAcceleration)
        {
            acceleration += Time.deltaTime;
        }
        else if (pState.xAxis == 0)
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
            acceleration = 0;
        }
        else if (acceleration > maxAcceleration)
        {
            acceleration -= Time.deltaTime;
        }
    }
    private void Running()
    {
        if (pState.running)
        {
            maxAcceleration = runMultiplier;
        }
        else
        {
            maxAcceleration = 1;
        }
    }
    private void SpeedConstraints()
    {
        if (pState.walled)
        {
            rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x, -maxSpeedX / 2, maxSpeedX / 2),
                                      Mathf.Clamp(rb.velocity.y, -wallSliding, maxSpeedY));
        }
        else if (pState.dashing)
        {
            rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x, -maxSpeedX * 2, maxSpeedX * 2), 0);
            Debug.Log("here");
        }
        else if(playerGroundedTimer > 0)
        {
            rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x, -maxSpeedX, maxSpeedX),
                                      Mathf.Clamp(rb.velocity.y, -maxSpeedY, maxSpeedY));
        }
        else //Move on Air
        {
            rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x, -maxSpeedX/2, maxSpeedX/2),
                                      Mathf.Clamp(rb.velocity.y, -maxSpeedY, maxSpeedY));
        }
    }
    private void MovementConstraints()
    {
        if (pState.dashing ||pState.walled ||pState.climb)
        {
            pState.canMove = false;
            pState.jumping = false;
        }
        else
        {
            pState.canMove = true;
        }
    }

    public void AddForce(float x, float y)
    {
        rb.AddForce(new Vector2(x, y), ForceMode2D.Impulse);
    }    
}
